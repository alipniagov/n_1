# Import Export Performance

Tools for measuring Project Import and Export performance.  
  
Test Imports run via scheduled pipelines: https://gitlab.com/gitlab-org/memory-team/import-export-performance/pipeline_schedules
  
Currently, we use https://gitlab.com/gitlab-org/quality/performance-data/raw/master/gitlabhq_export.tar.gz to measure the import and compare the results, but it could be set via the pipeline ENV vars.  
For now, we run the import on the https://staging.gitlab.com/ via API, and post the results to Slack (the channel is also set via pipeline schedules ENV vars).
